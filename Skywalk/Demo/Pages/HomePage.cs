﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Pages
{
    class HomePage
    {
        public IWebDriver WebDriver { get; }

        public HomePage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        public IWebElement linkProducts => WebDriver.FindElement(By.LinkText("PRODUCTS"));
        public IWebElement linkTequila => WebDriver.FindElement(By.LinkText("TEQUILA"));
        public IWebElement txtDisplayed => WebDriver.FindElement(By.CssSelector("h1"));

        public void ClickProducts() => linkProducts.Click();
        public void ClickTequila() => linkTequila.Click();
        public bool isTextExist() => txtDisplayed.Displayed;
    }
}
