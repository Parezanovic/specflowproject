﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Pages
{
    class ChilyPage
    {
        public IWebDriver WebDriver { get; }

        public ChilyPage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        public IWebElement GetsearchIcon()
        {
            var searchButtonClicked = WebDriver.FindElement(By.XPath("//*[@id='et_search_icon']"));
            Actions action = new Actions(WebDriver);
            action.MoveToElement(searchButtonClicked).Click().Build().Perform();
            return searchButtonClicked;
        }

        public IWebElement typing => WebDriver.FindElement(By.Name("s"));
        public IWebElement txtDisplayed3 => WebDriver.FindElement(By.CssSelector(".not-found-title"));

        public void ClickSearchIcon() => GetsearchIcon();
        public void TypingOnSearch() => typing.SendKeys("CHILY");
        public void ClickEnter() => typing.SendKeys(Keys.Enter);
        public bool isTextExist3() => txtDisplayed3.Displayed;
    }
}
