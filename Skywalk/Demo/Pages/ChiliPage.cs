﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Pages
{
    class ChiliPage
    {
        public IWebDriver WebDriver { get; }

        public ChiliPage(IWebDriver webDriver)
        {
            WebDriver = webDriver;
        }

        public IWebElement GetsearchIcon()
        {
            var searchButtonClicked = WebDriver.FindElement(By.XPath("//*[@id='et_search_icon']"));
            Actions action = new Actions(WebDriver);
            action.MoveToElement(searchButtonClicked).Click().Build().Perform();
            return searchButtonClicked;
        }

        public IWebElement typing => WebDriver.FindElement(By.Name("s"));
        public IWebElement txtDisplayed2 => WebDriver.FindElement(By.LinkText("CHILI4 – Limited Design – “Yellow”"));



        public void ClickSearchIcon() => GetsearchIcon();
        public void TypingOnSearch() => typing.SendKeys("CHILI");
        public void ClickEnter() => typing.SendKeys(Keys.Enter);
        public bool isTextExist2() => txtDisplayed2.Displayed;
    }
}
