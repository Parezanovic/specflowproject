﻿using Demo.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace Demo.Steps
{
    [Binding]
    class ChilySteps
    {
        ChilyPage chilyPage = null;

        [Given(@"Launch the application again again")]
        public void GivenLaunchTheApplicationAgainAgain()
        {
            IWebDriver webDriver = new ChromeDriver();
            webDriver.Navigate().GoToUrl("https://skywalk.info/");
            webDriver.Manage().Window.Maximize();
            chilyPage = new ChilyPage(webDriver);
        }

        [Given(@"search for chily")]
        public void GivenSearchForChily()
        {
            chilyPage.ClickSearchIcon();
            chilyPage.TypingOnSearch();
            chilyPage.ClickEnter();
        }

        [Then(@"Check URL and txt again")]
        public void ThenCheckURLAndTxtAgain()
        {
            Assert.That(chilyPage.isTextExist3(), Is.True);
        }


    }
}
