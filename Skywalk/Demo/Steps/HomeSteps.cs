﻿using Demo.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace Demo.Steps
{
    [Binding]
    class HomeSteps
    {
        HomePage homePage = null;

        [Given(@"Launch the application")]
        public void GivenLaunchTheApplication()
        {
            IWebDriver webDriver = new ChromeDriver();
            webDriver.Navigate().GoToUrl("https://skywalk.info/");
            homePage = new HomePage(webDriver);
        }

        [Given(@"In products click link tequil")]
        public void GivenInProductsClickLinkTequil()
        {
            homePage.ClickProducts();
            homePage.ClickTequila();
        }

        [Then(@"Check URL and")]
        public void ThenCheckURLAnd()
        {
            Assert.That(homePage.isTextExist(), Is.True);
        }
    }
}
