﻿using Demo.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;

namespace Demo.Steps
{
    [Binding]
    class ChiliSteps
    {
        ChiliPage chiliPage = null;

        [Given(@"Launch the application again")]
        public void GivenLaunchTheApplicationAgain()
        {
            IWebDriver webDriver = new ChromeDriver();
            webDriver.Navigate().GoToUrl("https://skywalk.info/");
            webDriver.Manage().Window.Maximize();
            chiliPage = new ChiliPage(webDriver);
        }

        [Given(@"search for chili")]
        public void GivenSearchForChili()
        {
            chiliPage.ClickSearchIcon();
            chiliPage.TypingOnSearch();
            chiliPage.ClickEnter();
        }

        [Then(@"Check URL and txt")]
        public void ThenCheckURLAndTxt()
        {
            Assert.That(chiliPage.isTextExist2(), Is.True);
        }

    }
}
